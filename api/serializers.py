from rest_framework import serializers

from .models import Sales, CardHolder


class APINewSalesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sales
        fields = '__all__'
        read_only_fields = ['invoice_no', ]


class APICardDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = CardHolder
        fields = '__all__'
        read_only_fields = ['invoice_no', ]
