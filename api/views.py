from django.shortcuts import render

# Create your views here.
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK
from rest_framework.views import APIView

from .models import Sales, CardHolder
from .serializers import APINewSalesSerializer, APICardDetailsSerializer


class APINewSales(generics.CreateAPIView):
    queryset = Sales.objects.all()
    serializer_class = APINewSalesSerializer


class APICardDetails(APIView):

    def get(self, request, *args, **kwargs):
        card_no = self.kwargs.get('card_no', None)
        # designation_id = self.kwargs.get('designation_id', None)
        print('Card No: ' + str(card_no))

        try:
            snippets = CardHolder.objects.get(card_no=card_no)
            # print(snippets)
            serializer = APICardDetailsSerializer(snippets)
            print(serializer.data.get('status'))
            if serializer.data.get('status'):
                return Response(serializer.data, status=HTTP_200_OK)
            else:
                return Response({'status': 'Card is not active!'}, status=status.HTTP_404_NOT_FOUND)

        except CardHolder.DoesNotExist:
            return Response({'status': 'No card found!'}, status=status.HTTP_404_NOT_FOUND)


class APIDailySalesList(generics.ListAPIView):
    queryset = Sales.objects.all()
    serializer_class = APINewSalesSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['xdate', 'phone', ]
