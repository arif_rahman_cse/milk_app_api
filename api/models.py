import datetime

from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class CardHolder(models.Model):
    # user = models.ForeignKey(User, on_delete=models.CASCADE)
    card_no = models.CharField(max_length=20, unique=True)
    chard_holder_name = models.CharField(max_length=100)
    address = models.TextField(null=True, blank=True, max_length=200)
    phone = models.CharField(null=True, blank=True, max_length=11)
    card_unite = models.DecimalField(max_digits=10, decimal_places=1)
    rate = models.DecimalField(max_digits=10, decimal_places=2)
    status = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return self.card_no + '-' + str(self.chard_holder_name)


def increment_invoice_number():
    # GET Current Date
    today = datetime.date.today()

    # Format the date like (20-11-28 YY-MM-DD)
    today_string = today.strftime('%d%m%y')

    # For the very first time invoice_number is DD-MM-YY-0001
    next_invoice_number = '0001'

    # Get Last Invoice Number of Current Year, Month and Day (29-11-21 DD-MM-YY)
    last_invoice = Sales.objects.filter(invoice_no__startswith=today_string).order_by('invoice_no').last()

    if last_invoice:
        # Cut 6 digit from the left and converted to int (201128:xxx)
        last_invoice_number = int(last_invoice.invoice_no[6:])

        # Increment one with last three digit
        next_invoice_number = '{0:04d}'.format(last_invoice_number + 1)

    # Return custom invoice number
    return today_string + next_invoice_number


class Sales(models.Model):
    invoice_no = models.CharField(max_length=10, default=increment_invoice_number, null=True, blank=True)
    card_holder = models.ForeignKey(CardHolder, on_delete=models.DO_NOTHING)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    card_no = models.CharField(max_length=20)
    chard_holder_name = models.CharField(max_length=100)
    address = models.TextField(null=True, blank=True, max_length=200)
    phone = models.CharField(null=True, blank=True, max_length=11)  # Scan User
    card_unite = models.DecimalField(max_digits=10, decimal_places=1)
    rate = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    total_qty = models.DecimalField(max_digits=10, decimal_places=2, )
    total_price = models.DecimalField(max_digits=10, decimal_places=2, )
    discount = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    number_of_scan = models.IntegerField()
    status = models.BooleanField(default=True)
    xdate = models.DateField(auto_now_add=True, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)
