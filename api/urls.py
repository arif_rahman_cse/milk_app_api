from django.urls import path, include

from .views import APINewSales, APICardDetails, APIDailySalesList

urlpatterns = [
    path('sales/new', APINewSales.as_view(), name='api-new-sales'),
    path('card/details/<str:card_no>', APICardDetails.as_view(), name='api-card-details'),
    path('daily-sales/list', APIDailySalesList.as_view(), name='api-daily-sales-list'),

]
