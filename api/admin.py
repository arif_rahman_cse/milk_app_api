from django.contrib import admin

# admin.site.register(Profile)

from django.contrib import admin

from api.models import CardHolder, Sales


@admin.register(CardHolder)
class CardHolderAdmin(admin.ModelAdmin):
    list_display = ['card_no', 'chard_holder_name', 'phone', 'address', 'card_unite', 'rate', ]
    list_per_page = 50
    search_fields = ['card_no', 'chard_holder_name']


@admin.register(Sales)
class SalesAdmin(admin.ModelAdmin):
    list_display = ['invoice_no', 'card_no', 'chard_holder_name', 'phone', 'card_unite', 'rate', 'total_qty', 'total_price',
                    'discount', 'number_of_scan', 'xdate', 'get_scanned_by']
    search_fields = ['invoice_no', 'xdate', 'card_no', ]
    list_per_page = 50

    def get_scanned_by(self, obj):
        return obj.user.username

    get_scanned_by.short_description = 'Scanned By'
    get_scanned_by.admin_order_field = 'user__username'
