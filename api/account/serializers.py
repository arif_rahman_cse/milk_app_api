from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.db.models import Q
from rest_framework import serializers
from rest_framework.exceptions import ValidationError


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email')


# User registration Serializer
class UserLoginSerializer(serializers.ModelSerializer):
    # token = serializers.CharField(allow_blank=True, read_only=True)
    username = serializers.CharField(required=False, allow_blank=True)
    email = serializers.EmailField(label='Email Address', required=False, allow_blank=True, read_only=True)
    first_name = serializers.CharField(required=False, allow_blank=True, allow_null=True, read_only=True)
    last_name = serializers.CharField(required=False, allow_blank=True, allow_null=True, read_only=True)
    is_active = serializers.BooleanField(read_only=True)

    class Meta:
        model = User
        fields = ['username', 'email', 'password', 'first_name', 'last_name', 'is_active']

        extra_kwargs = {
            'password': {'write_only': True},
        }

    def validate(self, value):
        print("***************validating*******************")
        data = self.get_initial()
        # user_obj = None
        email = data.get("email", None)
        username = data.get("username", None)
        password = data["password"]
        if not email and not username:
            raise ValidationError("A username or email is required to login.")
        user = User.objects.filter(
            Q(email=email) |
            Q(username=username)
        ).distinct()
        user = user.exclude(email__isnull=True).exclude(email__iexact='')
        print(user, "-------------*********************")
        if user.exists() and user.count() == 1:
            user_obj = user.first()
        else:
            raise ValidationError("Phone number is not valid.")
        if user_obj:
            if not user_obj.check_password(password):
                print("wrong pwd", "**************************************")
                raise ValidationError("Incorrect credentials! Please Try again!")

            if not user_obj.is_active:
                raise ValidationError("You are not a active user!")

        data["email"] = user_obj.email
        data["first_name"] = user_obj.first_name
        data["last_name"] = user_obj.last_name
        data["is_active"] = user_obj.is_active
        return data

    # Validate User Email
